package arp.message.kafka;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;

import arp.process.publish.Message;

import com.alibaba.fastjson.JSON;

public class FastjsonDeserializationStrategy implements
		KafkaMessageDeserializationStrategy<String> {

	@Override
	public void configValueDeserializerClass(Properties props) {
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				StringDeserializer.class);
	}

	@Override
	public Message deserialize(ConsumerRecord<String, String> record)
			throws Exception {
		return JSON.parseObject(record.value(), Message.class);
	}

}
