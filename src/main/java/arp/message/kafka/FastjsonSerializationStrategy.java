package arp.message.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import arp.process.publish.Message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class FastjsonSerializationStrategy implements
		KafkaMessageSerializationStrategy<String> {

	@Override
	public void configValueDeserializerClass(Properties props) {
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				StringSerializer.class);
	}

	@Override
	public ProducerRecord<String, String> serialize(String processDesc,
			Message message) throws Exception {
		return new ProducerRecord<>(processDesc, JSON.toJSONString(message,
				SerializerFeature.IgnoreNonFieldGetter));
	}

}
