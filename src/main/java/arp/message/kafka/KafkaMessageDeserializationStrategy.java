package arp.message.kafka;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import arp.process.publish.Message;

public interface KafkaMessageDeserializationStrategy<V> {
	void configValueDeserializerClass(Properties props);

	Message deserialize(ConsumerRecord<String, V> record) throws Exception;
}
