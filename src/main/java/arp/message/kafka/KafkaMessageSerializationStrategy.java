package arp.message.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerRecord;

import arp.process.publish.Message;

public interface KafkaMessageSerializationStrategy<V> {
	void configValueDeserializerClass(Properties props);

	ProducerRecord<String, V> serialize(String processDesc, Message message)
			throws Exception;
}
